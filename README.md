![FCTUNL](http://www.fct.unl.pt/sites/default/themes/fct_unl_pt_2015/images/logo.png)

# [Departamento de Informática](http://www.di.fct.unl.pt)

# Game Programming with Python - FCT

Fernando Birra (fpb@fct.unl.pt), João Costa Seco (joao.seco@fct.unl.pt), Miguel Goulão (mgoul@fct.unl.pt) and Vitor Duarte (vad@fct.unl.pt)

![Python](https://www.python.org/static/community_logos/python-logo-master-v3-TM.png)

In this activity we invite you to understand the nuts and bolts of a computer game and learn some basic programming concepts along the way. We challenge you to follow a sequence of simple steps that will guide you from the basic elements of a program written in the Python programming language, to the construction of a little computer game, reusing a set of available components.

# Programming 101

Let us start by talking a bit about programming. Is that even possible, in such a short amount of time?
To build a program is, essentially, to specify the behavior of a computer, in such a way that it fulfills our purposes. We use a programming language to write this specification. With this language, we are able to express this behavior using concepts like values, variables, functions, objects and classes. In this short tutorial, we will use the Python programming language. You can download Python tools from [www.python.org](https://www.python.org).

Most modern programs and computer systems are built from pre-fabricated software components, which are used as building blocks to create new programs. These building blocks are, themselves, software components. The development of a new program often involves a combination of pre-fabricated components with new components.

This week, we will do just that. We will start from the basics of programming. As we progress, we will take on increasingly complex challenges. In the end, we will use an existing "game engine" -- a set of pre-fabricated components -- to build... games! The game engine is called pygame and may be obtained from [https://www.pygame.org/](https://www.pygame.org/). 

But first things first...

## Printing messages on the console

### Hello, world!

The classic example of a simple program in any programming language is the famous Hello World. It goes like this: when you run the program, it prints the `"Hello, world!"` message on the screen.

~~~~
Hello, world!
~~~~

How can we do this with Python? It turns out it is quite simple to do it. You will need to use the `print` command. The `print` command prints something on the output (in this case, on a console). It receives one argument, with whatever you want to print. So, if you want to print the message `"Hello, world!"`, that is exactly what you need to provide as argument for the `print` command. In this case, the argument is a bit of text. We represent text as a **String**, in **Python**. The String is delimited by double quotes `"`.

To try this, we will need to open a Python console. There are many available. You can even run the console from the command line. However, as we are using a specific Integrated Development Environment (IDE) for Python named `Spyder`, we can use the Python console available within that environment. This console is named `IPython console`. To access it, please start your Spyder IDE. You will find the console on the right hand side of the IDE.

You should see a console with a text similar to this one (version details may be slightly different on your machine):

~~~~
Python 3.7.3 (default, Mar 27 2019, 17:13:21) [MSC v.1915 64 bit (AMD64)]
Type "copyright", "credits" or "license" for more information.

IPython 7.4.0 -- An enhanced Interactive Python.

In [1]: 
~~~~

The `In [1]:` prompts the user to type in a command. The number within brackets is just a counter. It will increment with every new command you input. Let us try out the `print` command. This command is implemented as a Python function. In Python, functions receive a list of arguments, within parenthesis. In this case, the argument contains the message that you want to print. This message is represented as a String, that is, a sequence of characters. Here, we want to print the String `"Hello, world!"`. Let us try this.

~~~~
Python 3.7.3 (default, Mar 27 2019, 17:13:21) [MSC v.1915 64 bit (AMD64)]
Type "copyright", "credits" or "license" for more information.

IPython 7.4.0 -- An enhanced Interactive Python.

In [1]:  print ("Hello, world!")
Hello, world!

In [2]:
~~~~

Note how the console has prompted you for the next command. This is how it works. Once it executes a command, it asks you for the next one.


### Printing the results of an expression

As the print command prints whatever is in between those quotes, if you write `print ("2 + 4")`, you will obtain as a result `2 + 4`, rather than `6`. What if you really want to print the result of an expression, such as `2 + 4`? Remember, the print operation prints what is inside of its arguments list. "Hello, world!", or "2 + 4", were expressions of the type String. You can use other expression types, such as expressions computing an integer value. To do so, simply remove the double quotes, as in the following example:

~~~~
In [2]: print ("2 + 4")
2 + 4

In [3]: print (2 + 4)
6
~~~~

In this case, the result to be printed is `6`. This is computed as follows: first, **Python** computes the value of the expression. Then, it prints that value. Of course, the expression must be a valid expression. For example, if you try to print `Hello, world!` without the double quotes, it does not work. **Python** does not know how to interpret this, so if you try it, you will get a message like this:

~~~~
In [4]: print Hello, world!
  File "<ipython-input-8-77eca6df1792>", line 1
    print Hello, world!
              ^
SyntaxError: Missing parentheses in call to 'print'. Did you mean print(Hello, world!)?
~~~~

If you are programming, every now and then these things will happen. 

**DON'T PANIC!!! :-)**

This is just the way Python has of telling you it does not understand the instruction you wrote. Computer programs have to be written following strict syntax rules. If we do not follow those rules, we will get error messages such as the one above.

Also, as **Python** cannot really guess what you want to do, it will try to make a suggestion, which may fix your problem, or not. In this case, it would not fix it, because `print(...)` needs something that can be converted to a String and `Hello, world!` can't. 

~~~~
In[5]: print (Hello, world!)
  File "<ipython-input-9-2f8c10d72de7>", line 1
    print (Hello, world!)
                       ^
SyntaxError: invalid syntax
~~~~

### Printing multiple items

Sometimes, you will want to print several items. You can do so by separating those items with commas, in the print instruction. Let us look at a few examples. Let's say you want to print `paper rock scisors` using three different Strings.

~~~~
In[6]: print ("paper", "rock", "scisors")
paper rock scisors
~~~~

What if you need to compute what you want to print? Suppose you want to print a message concerning how many people live in Snow White's house. There are 7 dwarves and Snow White, there. You can have python doing the math for you. An important detail is that the comma separating the arguments must be outside of the String(s). If you want a comma inside the String, you still need comma separating arguments outside of it (i.e. outside the double quotes).

~~~~
In[7]: print ("Inhabitants in Snow White's house", 7 + 1)
Inhabitants in Snow White's house 8

In[8]: print ("Inhabitants in Snow White's house,", 7 + 1)
Inhabitants in Snow White's house 8

In[9]: print ("Inhabitants in Snow White's house,", 7 + 1, "and", 2, "little birds")
Inhabitants in Snow White's house, 8 and 2 little birds

In[10]: print ("Inhabitants in Snow White's house," 7 + 1)
  File "<ipython-input-11-a806f87c9e50>", line 1
    print ("Inhabitants in Snow White's house," 7 + 1)
                                                ^
SyntaxError: invalid syntax
~~~~

You may also want to use special characters in your text. For example, you may want to write `You know what they say: "it ain't over until the fat lady sings..."`. But the double quote, as we have seen, is used as a delimiter for strings.

How can we put it *inside* the String? We need to use an *escape code*. Here is how it works: You use a special combination of charaters to denote the one you really want to write. You start with an *escape character*, namely `\`. Then, this is followed by an *escape code*. For example, for printing `"` you need to write `\"`. Putting it all together, here is what you get:

~~~~
In[11]: print ("You know what they say: \"it ain't over until the fat lady sings...\"")
You know what they say: "it ain't over until the fat lady sings..."
~~~~

There are several escape characters. For instance, use `\'` for a single quote `'`, `\"` for a double quote, `\t`, for a Tab, `\r`for a carriage return, or `\n` for a line feed (i.e., for changing to the next line). Let's play a bit with them to see their effect.

Some of these special characters are using differently depending on the operating system. For example, in Windows, the change of line is denoted by `\r\n`, while on Linux, and Mac OS this is denoted by `\n` and, in older Mac systems, this was denoted by `\r`.

~~~~
In[12]: print('apple\torange')
apple   orange

In[13]: print('apple\norange')
apple
orange
~~~~

Prefixing a special character with `\` turns it into a `normal` character. For example, `\'`is the single quote character. `'It\'s cold out there...'` is a valid String and is equivalent to `"It's cold out there..."`. Also, please note that `\` can be escaped by writing `\\`. Finally, surrounding a string with triple double-quotes `(""" """)` allows you to have any combination of quotes and line breaks within a string and **Python** will still interpret it as a single entity.

~~~~
In[14]: print ('Cristiano Ronaldo\'s hat trick\'s are awesome!') 
Cristiano Ronaldo's hat trick's are awesome!

In[15]: print ("Cristiano Ronaldo's hat trick's are awesome!")
Cristiano Ronaldo's hat trick's are awesome!

In[16]: print ("Both strings look the same, including the \\ridiculous spelling mistake\\!")
Both strings look the same, including the \ridiculous spelling mistake\!
~~~~

Try this, to see if you got it: can you write the short English poem in three separate lines, using a single `print()` function?

~~~~
In[17]: print("Fleas\nAdam\nHad'em")
Fleas
Adam
Had'em 
In[18]: print('Fleas\nAdam\nHad\'em')
Fleas
Adam
Had'em 
In[19]: print("""Fleas
Adam
Had'em""")
Fleas
Adam
Had'em 
~~~~

## Basic math operators
**Python** supports several basic math operators. In the next example, we make some simple computations involving integers. The operator `+` adds two operands. The operator `-` subtracts the first operand with the second one. The operation `*` multiplies two operands. The operator `/` computes the quotient between its left and right operand. The operator `//` computes the floor of the real division between its left and right operand (i.e. it computes the quotient). The operator `%` computes the remainder of its left operand divided by its right operand.  If both operands are integers, this is equivalent to using the `/` operator. The operator `**` computes the value of its first operand, raised to the power of its second operand. Finally, the unary `-` operator multiplies its operand by `-1`, thus changing its operand signal.

~~~~
In[20]: 2+3
5

In[21]: 2-3
-1

In[22]: 3*3
9

In[23]: 3/4
0.75

In[24]: 6/2
3.0

In[25]: 5//2
2

In[26]: 5%2
1

In[27]: 3**2
9

In[28]: -3
-3
~~~~

In the previous examples, we used integer operands. The same operators are also defined for real numbers. In **Python**, real numbers are expressed with the `.` symbol separating the integer from the decimal part of those numbers. If one computes an expression using values of two different types, the result is typically computed with the type of the most detailed type. Also, we need to keep in mind that real numbers have a finite representation in a computer so, sometimes, you may notice some imprecision in the results of basic computations. For example, adding `2.3` with `4.6` should equal to `6.9`. In fact it results in an imprecise result, as follows:

~~~~
In[29]: 2.3 + 4.6
6.8999999999999995
~~~~

Note that the values are pretty close, as the result can be rounded to `6.9`, the expected result. Nevertheless you should be aware that these imprecisions may occur and be prepared for them.

~~~~
In[30] 2.3 + 4.6
6.8999999999999995
~~~~


## Saving your programs

So far, we have been playing with **Python** on the console. This is convenient for trying out a few expressions, but not a good idea if you want build more complex programs. We should create files for keeping our source code. When creating **Python** files (which are often called **Python** scripts) use the `.py` extension.

If you look to the left hand side of your IDE, you will spot a source code editor which, by default, may be initiated with the following code:

~~~~
# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
~~~~

## Adding comments to your program
All of the code presented in the previous example illustrates comments in **Python**.

Use the character `#` to start a one line comment. The remainder of the line will be ignored by python.


~~~~python
print "This is not a comment"
# print "This is a comment"
print 3 # - 2
'''
print "the values in these lines"
print "are not printed"
print "as they are part of a multiline comment"
'''
print "got it?"
~~~~

The first line prints the string `"This is not a comment"`.
The second line is a comment, so it is not executed. 
The third line prints the value `3`.
The next five lines are comments, delimited from `'''` to `'''`.
Finally, the last line prints `got it?`.


## Variables, values and operations

**Python** allows you to manipulate values which may represent integers, real numbers, and so on. The games you are about to build will need those. They will involve some game rules, and physics. More importantly, building games is actually quite fun. Are you ready? Let's start...

The easiest way to learn about variables and their values is to play a bit with them. We will build and evaluate a few expressions using those variables.

Before we begin, we need a special program, called an "interpreter". This interpreter, in this case called **python** will allow us to run programs and play with those variables and expressions. Soon enough, we will be using the interpreter to run our games!

## Handling user input

To make this more interesting, we should make it interactive, so that the user can input data to our programs. Let us start with a very basic python script named `inputExample.py` that reads a number from the console, stores it in a variable named `value` and writes it back. The `input()` operation reads data from the console and returns a string with the read input.

~~~~
"""
This is a basic input output example.
Reads a number from the input and rewrites it on the output.
"""
value = input("Write a number: ")
print (value)
~~~~

To run the program, press the green arrowlike triangle (or go to the Run menu). This will the following effect in the console:

~~~~
In[31]: runfile('C:/Users/Miguel/Documents/FCT/Eventos/2019/CienciaViva2019/repository/inputExample.py', wdir='C:/Users/Miguel/Documents/FCT/Eventos/2019/CienciaViva2019/repository')

Write a number: 456
456
~~~~

Ok, so what if you write something other than a number?

~~~~
In [32]: runfile('C:/Users/Miguel/Documents/FCT/Eventos/2019/CienciaViva2019/repository/inputExample.py', wdir='C:/Users/Miguel/Documents/FCT/Eventos/2019/CienciaViva2019/repository')

Write a number: You know nothing, Jon Snow.
You know nothing, Jon Snow.
~~~~

As we did not specify what kind of input we were expecting, **Python** simply transformed our input into a String. If you really need an integer number, you should convert that String into a number, by using the `int()` function, which does just that.

~~~~
"""
This is a basic input output example.
Reads a number from the input and rewrites it on the output.
"""
value = int(input("Write a number: "))
print (value)
~~~~

Now, the value really must be an integer, or else... (go ahead, make my day. Are you feeling lucky?) :-)

Big crash, right? Nice. Value is supposed to be a String representing an integer number. If not, this function crashes. So, be careful when inputing values. We will postpone the necessary error handling for distracted users, for the time being. Let's continue.

Now that you know how to read a number, let's make this more interesting. 

The `mpgCalculatorInteractive.py` offers an example on how to read data from the user. Remember the role of each of the used functions:
The `input()` operation reads data from the console and returns a string with the read input. 
The `float()` operation turns that string into a real number. 
The `int()` operation turns that string into an integer value. Although not particularly useful in the `kinecticEnergy.py` example, the `str()` operation turns the value into a String.

~~~~
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  1 16:01:18 2019

@author: Miguel
"""

MAXIMUM = 6 # maximum allowed consumption
ECONOMIC = 4 # maximum allowed consumption for economic cars

distance = int(input("What is the total distance?\t"))
consumption = int(input("What is the total consumption?\t\t"))
base = 100.0 #km
#consumption for each 100 km

# 300 - 6
# 100 - x
# x = (100 * 6) / 300
carConsumption = (base * consumption) / distance
print ("consumption", carConsumption, "every", base, "km")
~~~~

Let's try it:

~~~~
runfile('C:/Users/Miguel/Documents/FCT/Eventos/2019/CienciaViva2019/repository/carConsumption.py', wdir='C:/Users/Miguel/Documents/FCT/Eventos/2019/CienciaViva2019/repository')

What is the total distance?	1000

What is the total consumption?		20
consumption 2.0 every 100.0 km
~~~~



### Control structures
Let us start with basic decisions, namely the `if` control statement. This statement has the syntax `if condition: statement` where the condition is evaluated to a boolean value, either `true` or `false`, and `statement` is only executed if `condition` is `true`. In this example, also note the assignment operator `=`, which assigns a value to a variable, and the relational operators(`<`, `<=`, `==`, `>=`, `>`, and `!=`).

~~~~
# Variables used in the example if statements
a = 4
b = 5
c = 6
 
# Basic comparisons
if a < b:
    print ("a is less than b")
 
if a > b:
    print ("a is greater than than b")
 
if a <= b:
    print ("a is less than or equal to b")
 
if a >= b:
    print ("a is greater than or equal to b")
 
# NOTE: It is very easy to mix when to use == and =.
# Use == if you are asking if they are equal, use =
# if you are assigning a value.
if a == b:
    print ("a is equal to b")
 
# Not equal
if a != b:
    print ("a and b are not equal")

~~~~

The following are the results from runing this script:

~~~~
runfile('C:/Users/Miguel/Documents/FCT/Eventos/2019/CienciaViva2019/repository/ifExampleComplete.py', wdir='C:/Users/Miguel/Documents/FCT/Eventos/2019/CienciaViva2019/repository')
a is less than b
a is less than or equal to b
a and b are not equal
~~~~

You may want to create composite boolean expressions. There are two values to consider: `True`and `False`. You can use the binary operators `and` and `or`, as well as the unary operator `not`. 

~~~~
# And
if a < b and a < c:
    print ("a is less than b and c")
 
# Non-exclusive or
if a < b or a < c:
    print ("a is less than either a or b (or both)")
 
 
# Boolean data type. This is legal!
a = True
if a:
    print ("a is true")
 
if not a:
    print ("a is false")
 
a = True
b = False
 
if a and b:
    print ("a and b are both true")
~~~~

This would produce the following output:

~~~~
a is less than b and c
a is less than either a or b (or both)
a is true
~~~~

In **Python** zero also means `False`. Conversely, every non-zero value is considered `True`.

~~~~
a = 3
b = 3
c = a == b
print(c)
 
# These are also legal and will trigger as being true because
# the values are not zero:
if 1:
    print ("1")
if "A":
    print ("A")
 
# This will not trigger as true because it is zero.
if 0:
    print ("Zero")
~~~~

This would result in:
~~~~
True
1
A
~~~~

Things can get tricky if you try to compare a variable to multiple values. Consider the following example where this comparison is first done the *wrong* way, and only then the *right* one:

~~~~
# Comparing variables to multiple values.
# The first if statement appears to work, but it will always
# trigger as true even if the variable a is not equal to b.
# This is because "b" by itself is considered true.
a = "c"
if a == "B" or "b":
    print ("a is equal to b. Maybe.")
 
# This is the proper way to do the if statement.
if a == "B" or a == "b":
    print ("a is equal to b.")
~~~~

It will produce the result:

~~~~
a is equal to b. Maybe.
~~~~



Then, move on to `ifElifExample.py`, to learn how to define alternatives. This introduces two new control statements: `elif` and `else`. 

~~~~
runfile('C:/Users/Miguel/Documents/FCT/Eventos/2019/CienciaViva2019/repository/ifElifElseExample.py', wdir='C:/Users/Miguel/Documents/FCT/Eventos/2019/CienciaViva2019/repository')
a < b
~~~~

Suppose now you want to build a small application that, given a temperature, will provide feedback concerning whether the temperature is too hot, or not. Start with a simple `if` statement.

~~~~
temperature = int(input("What is the temperature in Fahrenheit? "))
if temperature > 90:
    print ("It is hot outside")
print ("Done")
~~~~

This works fine for hot temperatures. But what if the temperature is not greater than 90? We need to introduce a new control command, called `else`, to allow for an alternative:

~~~~
temperature = int(input("What is the temperature in Fahrenheit? "))
if temperature > 90:
    print ("It is hot outside")
else:
    print ("It is not hot outside")
print ("Done")
~~~~

Sometimes you may need more than two options. You can use the `elif` (which stands for `else if`) control command:

~~~~
temperature = int(input("What is the temperature in Fahrenheit? "))
if temperature > 90:
    print ("It is hot outside")
elif temperature < 30:
    print ("It is cold outside")
else:
    print ("It is not hot outside")
print ("Done")
~~~~

The order in which you put the different alternatives matters. Can you spot what is wrong with the following code?

~~~~
temperature = int(input("What is the temperature in Fahrenheit? "))
if temperature > 90:
    print ("It is hot outside")
elif temperature > 110:
    print ("Oh man, you could fry eggs on the pavement!")
elif temperature < 30:
    print ("It is cold outside")
else:
    print ("It is ok outside")
print ("Done")
~~~~

You can make comparisons to other data types, not just with integers. For example, the next script makes a comparison with a String. 

~~~~
userName = input("What is your name? ")
if userName == "Jon Snow":
    print ("You know nothing, Jon Snow.")
else:
    print ("Nice meeting you,", userName)
print ("Done")
~~~~

If you input `Jon Snow`, the script behaves as follows:

~~~~
What is your name? Jon Snow
You know nothing, Jon Snow.
Done
~~~~

Otherwise, it will simply greet you back:

~~~~
What is your name? Fred
Nice meeting you, Fred
Done
~~~~

The `ifExampleComplete.py` example sums up all these control statements. It also illustrates how `1` can be interpreted as `true` and 0 can be interpreted as `false`. 

To control the flow of execution, we need to have other control statements beyond decisions. Often, we need to iterate over the members of a collection, or repeat a block of instructions a number of times. The `for` loop allows repeating a given set of instructions for a certain number of times, or while a control variable varies from a starting to an ending value.

~~~~
# Print 'Hi' 10 times
for i in range(10):
    print ("Hi")
 
# Print 'Hello' 5 times and 'There' once
for i in range(5):
    print ("Hello")
print ("There")
 
# Print 'Hello' 'There' 5 times
for i in range(5):
    print ("Hello")
    print ("There")
 
# Print the numbers 0 to 9
for i in range(10):
    print (i)
 
# Two ways to print the numbers 1 to 10
for i in range(1, 11):
    print (i)
 
for i in range(10):
    print (i + 1)
 
# Two ways to print the even numbers 2 to 10
for i in range(2, 12, 2):
    print (i)
 
for i in range(5):
    print ((i + 1) * 2)
 
# Count down from 10 down to 1 (not zero)
for i in range(10, 0, -1):
    print (i)
 
# Print numbers out of a list
for i in [2, 6, 4, 2, 4, 6, 7, 4]:
    print (i)
 
# What does this print? Why?
for i in range(3):
    print ("a")
    for j in range(3):
        print("b")
 
# What is the value of a?
a = 0
for i in range(10):
    a = a + 1
print (a)
 
# What is the value of a?
a = 0
for i in range(10):
    a = a + 1
for j in range(10):
    a = a + 1
print (a)
 
# What is the value of a?
a = 0
for i in range(10):
    a = a + 1
    for j in range(10):
        a = a + 1
print (a)
 
# What is the value of sum?
sum = 0
for i in range(1, 101):
    sum = sum + i
print (sum)
~~~~

This example is available in the file `forLoopExample.py`

There is another way of repeating instruction blocks in **Python**, while a given condition is `True`. You may use a `while` loop.

~~~~
# A while loop can be used anywhere a for loop is used:
i = 0
while i < 10:
    print (i)
    i = i + 1
 
# This is the same as:
for i in range(10):
    print (i)
 
# It is possible to short hand the code:
# i = i + 1
# With the following:
# i += 1
# This can be done with subtraction, and multiplication as well.
i = 0
while i < 10:
    print (i)
    i += 1
 
# What would this print?
i = 1
while i <= 2**32:
    print (i)
    i *= 2
 
# A very common operation is to loop until the user performs
# a request to quit
quit = "n"
while quit == "n":
    quit = input ("Do you want to quit? ")

print ("looser")
 
# There may be several ways for a loop to quit. Using a boolean
# to trigger the event is a way of handling that.
done = False
while not done:
    quit = input ("Do you want to quit? ")
    if quit == "y":
        done = True
 
    attack = input ("Does your elf attack the dragon? ")
    if attack == "y":
        print ("Bad choice, you died.")
        done = True
 
value = 0
increment = 0.5
while value < 0.999:
    value += increment
    increment *= 0.5
    print (value)
 
# -- Common problems with while loops --
 
# The programmer wants to count down from 10
# What is wrong and how to fix it?
i = 10
while i == 0:
    print (i)
    i -= 1
 
# What is wrong with this loop that tries
# to count to 10? What will happen when it is run?
i = 1
while i < 10:
    print (i)
~~~~

Ok, now we have almost all the ingredients for creating a first game: a guessing number game. Suppose that you want to guess a number between 0 and 100.

In order to make things interesting, we will need to generate a random number before we start a new game. Fortunately, **Python** already has a library that can help us with generating random numbers. Conveniently, it is named `random`. We need to import it and then use it to generate a random number. Here are a few examples of how a random number can be generated.

~~~~
import random

#Random number from 0 to 49
number = random.randrange(50)
print (number)

#Random number from 100 to 200
otherNumber = random.randrange(100, 201)
print (otherNumber)

#Picking a random item out of a list
my_list = ["rock", "paper", "scissors"]
random_index = random.randrange(3)
print (my_list[random_index])

#Random floating point number from 0 to 1
my_number = random.random()
print (my_number)

#Random floating point number between 10 and 15
my_number = random.random() * 5 + 10
print (my_number)
~~~~

This would produce the following result:

~~~~
runfile('C:/Users/Miguel/Documents/FCT/Eventos/2019/CienciaViva2019/repository/randomValueExample.py', wdir='C:/Users/Miguel/Documents/FCT/Eventos/2019/CienciaViva2019/repository')
22
120
scissors
0.4819399167246906
10.42450034976861
~~~~

Note that the random values presented in the output would vary from one execution to the next.

### Operations
Please have a look at the example `mathFunctions.py`

~~~~
def maximum (x, y):
    if x > y:
        return x
    else:
        return y

def sum (x, y):
    return x + y

a = int(input ("write a number:\t"))
b = int(input ("write another number:\t"))
print ("the maximum is", maximum (a, b))
print ("the sum is", sum(a,b))
~~~~

The output is:

~~~~
runfile('C:/Users/Miguel/Documents/FCT/Eventos/2019/CienciaViva2019/repository/mathFunctions.py', wdir='C:/Users/Miguel/Documents/FCT/Eventos/2019/CienciaViva2019/repository')

write a number:	2

write another number:	3
the maximum is 3
the sum is 5
~~~~

Please have a look at the example `factorial.py`

~~~~
# Este exemplo demonstra como se pode declarar e usar uma funcao

# Funcao factorial
# Args: x (deve ser um valor inteiro)
def factorial(x):
	resultado = 1

	while x >= 1:
		resultado = resultado * x
		x = x-1

	return resultado



#programa principal
n = int(input ("escreva um n:"))
print (factorial(n))
~~~~

A sample output would be:
~~~~
runfile('C:/Users/Miguel/Documents/FCT/Eventos/2019/CienciaViva2019/repository/factorial.py', wdir='C:/Users/Miguel/Documents/FCT/Eventos/2019/CienciaViva2019/repository')

escreva um n:5
120
~~~~



## Collections
### Lists
Please have a look at the example `listas.py`

~~~~
def imprime(lista):
	for item in lista:
		print (item)	

def imprimeComWhile(lista):
	indice = 0
	while indice < len(lista):
		print (lista[indice])
		indice = indice + 1 

def imprimeAlfabeticaInversa(lista):
	lista.sort()
	indice = len(lista) - 1
	while indice >= 0:
		print (lista[indice])
		indice = indice - 1

def compra(lista, indice):
	itemComprado = lista[indice]
	del lista[indice]
	return itemComprado

def apaga(lista):
	while len(lista) > 0:
		del lista[0]

##########################

listaCompras = ["banana", "tv", "pera","cebola", "cenoura"]

print ("imprime com for")
imprime(listaCompras)

# O exemplo com while serve para mostrar como o for each pode ser feito 
# com um while
# print ("\nimprime com while")
# imprimeComWhile(listaCompras)

print ("faz o resto...")
listaCompras.append("bimby")


print ("comprei\n", compra(listaCompras, 3))

imprime(listaCompras)

listaCompras.sort()

imprime(listaCompras)

print ("primeiro elemento da lista de compras: ", listaCompras[1])
~~~~

It's possible to "slice" a list in Python. This will return a specific segment of a given list. For example, the command `myList[2]` will return the 3rd item in your list (remember that **Python** begins counting with the number 0).

You can expand the length and change the location of the list segment returned using a colon in your command. Ex: `myList[2:5]` will return the 3rd through 5th items in your list. Typing `myList[:5]` will return every item up to the 5th item on a list, while `myList[2:]` on the other hand, with a colon following the numeral, will return every item starting with the 3rd.

**Python** can also return a segment of a list counting from the end. This is done simply by inserting a negative before the desired numerals within the slice command. Ex: `myList[-5]` will return your fifth from last entry.

Example: 
~~~~
>>> w = 'python'
>>> w[-1]   # first character from the end
'n' 
>>> w[-2]   # second character from the end
'o' 
>>> w[1:3]
'yt'
>>> w[2:5]
'tho' 
>>> w[:5]   # slice from the beginning
'pytho' 
>>> w[2:]   # slice until the end
'thon'
>>> w[-3]
'h' 
>>> w[-3:]   # negative index can also be used in slicing
'hon' 
~~~~

The list is `fox = ['the', 'quick', 'brown', 'fox', 'jumps', 'over']`. How do you get `['quick', 'brown', 'fox']` from this list, using slicing?

~~~~
>>> fox = ['the', 'quick', 'brown', 'fox', 'jumps', 'over'] 
>>> fox[1:4]
['quick', 'brown', 'fox'] 
>>> fox[1:-2]
['quick', 'brown', 'fox'] 
>>> 
~~~~

Using slicing, how can you get 'dental' from 'incidentally'? How about 'action' from 'traction'?

~~~~
>>> 'incidentally'[4:10]
'dental' 
>>> 'incidentally'[4:-2]
'dental' 
>>> 'traction'[2:10]
'action' 
>>> 'traction'[2:]
'action'
>>> 
~~~~

### Tupples
Python provides another type that is an ordered collection of objects, called a tuple.
Tuples are identical to lists in all respects, except they are defined by enclosing the elements in parentheses (()) instead of square brackets ([]). Also, tuples are immutable.
Here is a short example showing a tuple definition, indexing, and slicing:

~~~~
t = ('foo', 'bar', 'baz', 'qux', 'quux', 'corge')
>>> t
('foo', 'bar', 'baz', 'qux', 'quux', 'corge')

>>> t[0]
'foo'
>>> t[-1]
'corge'
>>> t[1::2]
('bar', 'qux', 'corge')
~~~~

Why use a tuple instead of a list?

Program execution is faster when manipulating a tuple than it is for the equivalent list. (This is probably not going to be noticeable when the list or tuple is small.)

Sometimes you don’t want data to be modified. If the values in the collection are meant to remain constant for the life of the program, using a tuple instead of a list guards against accidental modification.

There is another Python data type that you will encounter shortly called a dictionary, which requires as one of its components a value that is of an immutable type. A tuple can be used for this purpose, whereas a list can’t be.

*Tuple Assignment, Packing, and Unpacking*

As you have already seen above, a literal tuple containing several items can be assigned to a single object:
~~~~
>>> t = ('foo', 'bar', 'baz', 'qux')
>>> t[0]
'foo'
>>> t[-1]
'qux'
~~~~

If that “packed” object is subsequently assigned to a new tuple, the individual items are “unpacked” into the objects in the tuple:

~~~~
>>> (s1, s2, s3, s4) = t
>>> s1
'foo'
>>> s2
'bar'
>>> s3
'baz'
>>> s4
'qux'
~~~~

When unpacking, the number of variables on the left must match the number of values in the tuple:

~~~~
>>> (s1, s2, s3) = t
Traceback (most recent call last):
  File "<pyshell#16>", line 1, in <module>
    (s1, s2, s3) = t
ValueError: too many values to unpack (expected 3)

>>> (s1, s2, s3, s4, s5) = t
Traceback (most recent call last):
  File "<pyshell#17>", line 1, in <module>
    (s1, s2, s3, s4, s5) = t
ValueError: not enough values to unpack (expected 5, got 4)
~~~~

Packing and unpacking can be combined into one statement to make a compound assignment:

~~~~
>>> (s1, s2, s3, s4) = ('foo', 'bar', 'baz', 'qux')
>>> s1
'foo'
>>> s2
'bar'
>>> s3
'baz'
>>> s4
'qux'
~~~~

Again, the number of elements in the tuple on the left of the assignment must equal the number on the right:

~~~~
>>> (s1, s2, s3, s4, s5) = ('foo', 'bar', 'baz', 'qux')
Traceback (most recent call last):
  File "<pyshell#63>", line 1, in <module>
    (s1, s2, s3, s4, s5) = ('foo', 'bar', 'baz', 'qux')
ValueError: not enough values to unpack (expected 5, got 4)
~~~~

In assignments like this and a small handful of other situations, Python allows the parentheses that are usually used for denoting a tuple to be left out:

~~~~
>>> t = 1, 2, 3
>>> t
(1, 2, 3)

>>> x1, x2, x3 = t
>>> x1, x2, x3
(1, 2, 3)

>>> x1, x2, x3 = 4, 5, 6
>>> x1, x2, x3
(4, 5, 6)

>>> t = 2,
>>> t
(2,)
~~~~

It works the same whether the parentheses are included or not, so if you have any doubt as to whether they’re needed, go ahead and include them.

Tuple assignment allows for a curious bit of idiomatic Python. Frequently when programming, you have two variables whose values you need to swap. In most programming languages, it is necessary to store one of the values in a temporary variable while the swap occurs like this:

~~~~
>>> a = 'foo'
>>> b = 'bar'
>>> a, b
('foo', 'bar')

>>># We need to define a temp variable to accomplish the swap.
>>> temp = a
>>> a = b
>>> b = temp

>>> a, b
('bar', 'foo')
~~~~

In Python, the swap can be done with a single tuple assignment:

~~~~
>>> a = 'foo'
>>> b = 'bar'
>>> a, b
('foo', 'bar')

>>># Magic time!
>>> a, b = b, a

>>> a, b
('bar', 'foo')
~~~~

As anyone who has ever had to swap values using a temporary variable knows, being able to do it this way in Python is the pinnacle of modern technological achievement. It will never get better than this.


### Maps / Dictionaries

`dict (dictionary)`, dictionary has *keys* and *values* for those keys. You can declare a dictionary with its starting entries. 

~~~~
>>> simpsons = {'Homer':36, 'Marge':35, 'Bart':10}
~~~~

You can update a dictionary with a new entry/value.

~~~~
>>> simpsons['Lisa'] = 8              # Add a new entry
>>> simpsons
{'Homer': 36, 'Lisa': 8, 'Marge': 35, 'Bart': 10} 
~~~~

You can update a value in the dictionary:

~~~~
>>> simpsons['Marge'] = 36            # Update value for key 'Marge'
>>> simpsons
{'Homer': 36, 'Lisa': 8, 'Marge': 36, 'Bart': 10} 
~~~~

You can obtain a list of the keys and values in a dictionary, in no particular order:

~~~~
>>> simpsons.keys()         # .keys() returns list-like object consisting of keys
dict_keys(['Homer', 'Lisa', 'Marge', 'Bart'])
>>> simpsons.values()       # .values() returns list-like object consisting of values
dict_values([36, 8, 35, 10])
~~~~

`.keys()` returns the keys in the dictionary as a list (sort of), and `.values()` returns the dictionary values as a list (sort of). Why "sort of"? Because the returned objects are not a list per se -- they are their own list-like types, called `dict_keys` and `dict_values`.

On the other hand, .items() returns both keys and values, but as a quasi-list of (key, value) tuples:

~~~~
>>> simpsons.items()        # .items() returns a quasi-list of key, value tuples
dict_items([('Homer', 36), ('Lisa', 8), ('Marge', 35), ('Bart', 10)])
~~~~

Dictionaries are orderless, but often it's necessary to impose a certain order (e.g., alphabetical) when processing dictionary entries. You achieve this by sorting the keys. 

Add Maggie and Grandpa Abe Simpson to the simpsons dictionary. Their respective ages: 1 and 65. 

~~~~
>>> simpsons
{'Bart': 10, 'Lisa': 8, 'Homer': 36, 'Marge': 36} 
>>> simpsons['Abe'] = 65
>>> simpsons
{'Bart': 10, 'Abe': 65, 'Lisa': 8, 'Homer': 36, 'Marge': 36} 
>>> simpsons['Maggie'] = 1
>>> simpsons
{'Maggie': 1, 'Abe': 65, 'Bart': 10, 'Lisa': 8, 'Homer': 36, 'Marge': 36} 
>>> simpsons['Abe']
65 
>>> 
~~~~


Please have a look at the example `addressBook.py`.

~~~~
agenda = {
	'vitor': 'vad@fct.unl.pt',
	'miguel': 'mgoul@fct.unl.pt',
	'jose': 'jose@campus.fct.unl.pt',
	'lara': 'lara@croft.com' 
}

print "A agenda tem", len(agenda), "contactos"

nome = raw_input ("Que endereco email quer? ")

if nome in agenda:
	print "O email que procura e: ", agenda[nome]
else:
	print "Nao temos o endereco de ", nome
~~~~

## Classes

### Class
Please have a lool at the example `WalkingDead.py`

~~~~
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  3 00:52:55 2019

@author: Miguel
"""

VIVO = 0
ZOMBIE = 1
MORTO = 2

class Pessoa():
	def __init__(self, nome, estado):
		self.nome = nome
		self.estado = estado

	def falar(self):
		if self.estado == VIVO:
			print ("Estou vivinho da Silva e o meu nome e", self.nome)
		elif self.estado == ZOMBIE:
			print ("iuoytrewgjreglmvv")
		else:
			print ("...")

	def mordido(self):
		if self.estado == VIVO:
			self.estado = ZOMBIE

	def curado(self):
		if self.estado == ZOMBIE:
			self.estado = VIVO

	def morto(self):
		if self.estado == VIVO or self.estado ==ZOMBIE:
			self.estado = MORTO

	def melisandrar(self):
		self.estado = VIVO

laura = Pessoa("Laura Palmer", MORTO)
zeca = Pessoa("zeca diabo", ZOMBIE)
laura.falar()
zeca.falar()
zeca.mordido()
zeca.falar()
zeca.curado()
zeca.falar()
zeca.morto()
zeca.falar()
laura.falar()
~~~~

The result is:

~~~~
...
iuoytrewgjreglmvv
iuoytrewgjreglmvv
Estou vivinho da Silva e o meu nome e zeca diabo
...
...
~~~~

Please have a look at the example `personagem.py`

~~~~
# Definicao da classe
# meuNome    - nome da personagem
# meusPontos - pontos de vida

class Personagem():
    # Construtor
    def __init__(self, nome, pontos, forca):
        self.meuNome = nome
        self.meusPontos = pontos
        self.forca = forca
    
    #devolve o nome da personagem
    def devolveNome(self):
        return self.meuNome
    
    #devolve o numero de pontos da personagem
    def devolvePontos(self):
        return self.meusPontos
    
    #devolve verdade se estiver KO, ou falso, caso contrario
    def estaKO(self):
        return self.meusPontos <= 0
    
    #esmurrar personagem
    def levaUmaPera(self, forcaPera):
        self.meusPontos = self.meusPontos - forcaPera
        
    def daPera(self):
        return self.forca

################################################
# Programa principal
################################################

rocky = Personagem("Rocky Balboa", 100, 20)
ivan = Personagem("Ivan Drago", 120, 15)

peras = 0
while rocky.estaKO() != True and ivan.estaKO() != True:
    rocky.levaUmaPera(ivan.daPera())
    ivan.levaUmaPera(rocky.daPera())
    peras = peras + 1
    print rocky.devolvePontos(), "\t", ivan.devolvePontos()
    
if rocky.estaKO():
    print "aargh em ingles"

if ivan.estaKO():
    print "aargh em russo"
~~~~

Please have a look at the example `DinossaurosV2.py`

~~~~
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  2 23:21:45 2019

@author: Miguel
"""

class Dinossauro():
    def __init__(self, nome, peso, pesoMinimo):
        self.nome = nome
        self.peso = peso
        self.pesoMinimo = pesoMinimo
        
    def devolveNome(self):
        return self.nome
    
    def devolvePeso(self):
        return self.peso
    
    def come(self, kilos):
        if (self.estaVivo()):
            self.peso = self.peso + kilos

    def perdePeso(self, kilos):
        if (self.estaVivo()):
            self.peso = self.peso - kilos
        
    def estaVivo(self):
        return self.peso >= self.pesoMinimo
    
    def estaMorto(self):
        return not self.estaVivo()
    
    def comeDinossauro(self, outroDinossauro):
        if(self.estaVivo()):
            self.come(outroDinossauro.devolvePeso())
            outroDinossauro.perdePeso(outroDinossauro.devolvePeso())
   

class Dinossaura(Dinossauro):
    def __init__(self, nome, peso, pesoMinimo):
        Dinossauro.__init__(self, nome, peso, pesoMinimo)
        
    def temRazao(self):
        return True
    
    def poeOvo(self):
        self.perdePeso(20)
        return Dinossauro("bebe dino", 20, 20)
    
class DinossauroMachoLatino(Dinossauro):
    def __init__(self, nome, peso, pesoMinimo):
        Dinossauro.__init__(self, nome, peso, pesoMinimo)
        
    def temRazao(self):
        return False
    
    def ultimaPalavra(self):
        return "Sim, querida!"
        
    
#########################################

bob = DinossauroMachoLatino("Bob", 60, 50)
print (bob.ultimaPalavra())
print (bob.devolveNome())
print (bob.devolvePeso())
bob.come(10)
print (bob.devolvePeso())
bob.come(100)
print (bob.devolvePeso())

alfredo = DinossauroMachoLatino("Alfredo Jr.", 900, 750)
print (alfredo.devolveNome())
print (alfredo.devolvePeso())

alfredo.comeDinossauro(bob)

print (alfredo.devolveNome(), "pesa", alfredo.devolvePeso())
print (bob.devolveNome(), "pesa", bob.devolvePeso())
print (bob.estaVivo())
print (bob.estaMorto())
bob.comeDinossauro(alfredo)
print (bob.devolveNome(), "pesa", bob.devolvePeso())
print (bob.estaVivo())
print (bob.estaMorto())

gregoria = Dinossaura ("Gregoria Alexandra", 100, 70)
francisco = gregoria.poeOvo()
print (francisco.devolveNome(), francisco.devolvePeso())
~~~~

Here are the results:

~~~~
Sim, querida!
Bob
60
70
170
Alfredo Jr.
900
Alfredo Jr. pesa 1070
Bob pesa 0
False
True
Bob pesa 0
False
True
bebe dino 20
~~~~

Please have a look at the example `walkingDead.py`

### Inheritance
Please have a look at the example `sharknado.py`



# References

[pygame] (https://www.panda3d.org)

[Program Arcade Games With Python And Pygame](http://programarcadegames.com/index.php?lang=en)

[Python 3 Notes] (https://www.pitt.edu/~naraehan/python3/) 
